#this file implements the functions related to PhysRevLett.82.2143


"""
   get_fermi_function
   simple fermi function calculation
"""
function get_fermi_function(ε::Float64, µ::Float64, β::Float64)
   return 1/(exp(β*(ε-µ))+1)
end


"""
   get_F_tilde
   see formula after eq. 6 of above paper. we have µ = +/- eV/2 
"""
function get_F_tilde(ε::Float64, µ::Float64, β::Float64)
    term = 0.5*β*(exp(β*(ε-µ/2))/(exp(β*(ε-µ/2))+1)^2 + 
                 exp(β*(ε+µ/2))/(exp(β*(ε+µ/2))+1)^2)
    return term
end

"""
   get_dε_F_tilde
   calculate dF_tilde/ddε of avove formula 
"""
function get_dε_F_tilde(ε::Float64, µ::Float64, β::Float64)
    term1 = -0.5*(2*β^2*exp(2*β*(ε-µ/2))/(exp(β*(ε-µ/2))+1)^3 -
                  β^2*exp(β*(ε-µ/2))/(exp(β*(ε-µ/2))+1)^2)
    term2 = -0.5*(2*β^2*exp(2*β*(ε+µ/2))/(exp(β*(ε+µ/2))+1)^3 -
                  β^2*exp(β*(ε+µ/2))/(exp(β*(ε+µ/2))+1)^2)
    return term1 + term2
end